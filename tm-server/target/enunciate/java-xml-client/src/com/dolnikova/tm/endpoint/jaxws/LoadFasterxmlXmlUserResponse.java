/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package com.dolnikova.tm.endpoint.jaxws;

import javax.xml.namespace.QName;

/**
 * Response bean for the loadFasterxmlXmlUserResponse operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "loadFasterxmlXmlUserResponse",
  namespace = "http://endpoint.tm.dolnikova.com/"
)
@javax.xml.bind.annotation.XmlType (
  name = "loadFasterxmlXmlUserResponse",
  namespace = "http://endpoint.tm.dolnikova.com/",
  propOrder = { "_retval" }
)
 @javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class LoadFasterxmlXmlUserResponse {

  @javax.xml.bind.annotation.XmlElement (
    name = "return"
  )
  protected java.util.List<com.dolnikova.tm.entity.User> _retval;

  /**
   * 
   */
  public java.util.List<com.dolnikova.tm.entity.User> getReturn() {
    return this._retval;
  }

  /**
   * 
   */
  public void setReturn(java.util.List<com.dolnikova.tm.entity.User> value) {
    this._retval = value;
  }

}
