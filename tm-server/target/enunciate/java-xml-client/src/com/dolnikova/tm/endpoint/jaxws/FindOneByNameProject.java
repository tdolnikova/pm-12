/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package com.dolnikova.tm.endpoint.jaxws;

import javax.xml.namespace.QName;

/**
 * Request bean for the findOneByNameProject operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "findOneByNameProject",
  namespace = "http://endpoint.tm.dolnikova.com/"
)
@javax.xml.bind.annotation.XmlType (
  name = "findOneByNameProject",
  namespace = "http://endpoint.tm.dolnikova.com/",
  propOrder = { "session", "name" }
)
@javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class FindOneByNameProject {

  @javax.xml.bind.annotation.XmlElement (
    name = "session"
  )
  protected com.dolnikova.tm.entity.Session session;
  @javax.xml.bind.annotation.XmlElement (
    name = "name"
  )
  protected java.lang.String name;

  /**
   * 
   */
  public com.dolnikova.tm.entity.Session getSession() {
    return this.session;
  }

  /**
   * 
   */
  public void setSession(com.dolnikova.tm.entity.Session session) {
    this.session = session;
  }

  /**
   * 
   */
  public java.lang.String getName() {
    return this.name;
  }

  /**
   * 
   */
  public void setName(java.lang.String name) {
    this.name = name;
  }
}
