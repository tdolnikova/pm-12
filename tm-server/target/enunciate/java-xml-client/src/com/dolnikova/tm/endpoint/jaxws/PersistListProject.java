/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package com.dolnikova.tm.endpoint.jaxws;

import javax.xml.namespace.QName;

/**
 * Request bean for the persistListProject operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "persistListProject",
  namespace = "http://endpoint.tm.dolnikova.com/"
)
@javax.xml.bind.annotation.XmlType (
  name = "persistListProject",
  namespace = "http://endpoint.tm.dolnikova.com/",
  propOrder = { "session", "list" }
)
@javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class PersistListProject {

  @javax.xml.bind.annotation.XmlElement (
    name = "session"
  )
  protected com.dolnikova.tm.entity.Session session;
  @javax.xml.bind.annotation.XmlElement (
    name = "list"
  )
  protected java.util.List<com.dolnikova.tm.entity.Project> list;

  /**
   * 
   */
  public com.dolnikova.tm.entity.Session getSession() {
    return this.session;
  }

  /**
   * 
   */
  public void setSession(com.dolnikova.tm.entity.Session session) {
    this.session = session;
  }

  /**
   * 
   */
  public java.util.List<com.dolnikova.tm.entity.Project> getList() {
    return this.list;
  }

  /**
   * 
   */
  public void setList(java.util.List<com.dolnikova.tm.entity.Project> list) {
    this.list = list;
  }
}
