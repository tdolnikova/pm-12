/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package com.dolnikova.tm.endpoint.jaxws;

import javax.xml.namespace.QName;

/**
 * Request bean for the checkPassword operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "checkPassword",
  namespace = "http://endpoint.tm.dolnikova.com/"
)
@javax.xml.bind.annotation.XmlType (
  name = "checkPassword",
  namespace = "http://endpoint.tm.dolnikova.com/",
  propOrder = { "session", "userInput" }
)
@javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class CheckPassword {

  @javax.xml.bind.annotation.XmlElement (
    name = "session"
  )
  protected com.dolnikova.tm.entity.Session session;
  @javax.xml.bind.annotation.XmlElement (
    name = "userInput"
  )
  protected java.lang.String userInput;

  /**
   * 
   */
  public com.dolnikova.tm.entity.Session getSession() {
    return this.session;
  }

  /**
   * 
   */
  public void setSession(com.dolnikova.tm.entity.Session session) {
    this.session = session;
  }

  /**
   * 
   */
  public java.lang.String getUserInput() {
    return this.userInput;
  }

  /**
   * 
   */
  public void setUserInput(java.lang.String userInput) {
    this.userInput = userInput;
  }
}
