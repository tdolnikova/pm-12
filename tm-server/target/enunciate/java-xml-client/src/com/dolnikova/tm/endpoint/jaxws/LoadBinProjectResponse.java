/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package com.dolnikova.tm.endpoint.jaxws;

import javax.xml.namespace.QName;

/**
 * Response bean for the loadBinProjectResponse operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "loadBinProjectResponse",
  namespace = "http://endpoint.tm.dolnikova.com/"
)
@javax.xml.bind.annotation.XmlType (
  name = "loadBinProjectResponse",
  namespace = "http://endpoint.tm.dolnikova.com/",
  propOrder = { "_retval" }
)
 @javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class LoadBinProjectResponse {

  @javax.xml.bind.annotation.XmlElement (
    name = "return"
  )
  protected java.util.List<com.dolnikova.tm.entity.Project> _retval;

  /**
   * 
   */
  public java.util.List<com.dolnikova.tm.entity.Project> getReturn() {
    return this._retval;
  }

  /**
   * 
   */
  public void setReturn(java.util.List<com.dolnikova.tm.entity.Project> value) {
    this._retval = value;
  }

}
