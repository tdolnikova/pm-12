/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package com.dolnikova.tm.endpoint.jaxws;

import javax.xml.namespace.QName;

/**
 * Response bean for the loadBinTaskResponse operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "loadBinTaskResponse",
  namespace = "http://endpoint.tm.dolnikova.com/"
)
@javax.xml.bind.annotation.XmlType (
  name = "loadBinTaskResponse",
  namespace = "http://endpoint.tm.dolnikova.com/",
  propOrder = { "_retval" }
)
 @javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class LoadBinTaskResponse {

  @javax.xml.bind.annotation.XmlElement (
    name = "return"
  )
  protected java.util.List<com.dolnikova.tm.entity.Task> _retval;

  /**
   * 
   */
  public java.util.List<com.dolnikova.tm.entity.Task> getReturn() {
    return this._retval;
  }

  /**
   * 
   */
  public void setReturn(java.util.List<com.dolnikova.tm.entity.Task> value) {
    this._retval = value;
  }

}
