/**
 * 
 *
 * Generated by <a href="http://enunciate.webcohesion.com">Enunciate</a>.
 */
package com.dolnikova.tm.endpoint.jaxws;

import javax.xml.namespace.QName;

/**
 * Response bean for the saveFasterxmlJsonTaskResponse operation.
 */
@javax.xml.bind.annotation.XmlRootElement (
  name = "saveFasterxmlJsonTaskResponse",
  namespace = "http://endpoint.tm.dolnikova.com/"
)
@javax.xml.bind.annotation.XmlType (
  name = "saveFasterxmlJsonTaskResponse",
  namespace = "http://endpoint.tm.dolnikova.com/",
  propOrder = { "_retval" }
)
 @javax.xml.bind.annotation.XmlAccessorType ( javax.xml.bind.annotation.XmlAccessType.FIELD )
public class SaveFasterxmlJsonTaskResponse {

  @javax.xml.bind.annotation.XmlElement (
    name = "return"
  )
  protected boolean _retval;

  /**
   * 
   */
  public boolean getReturn() {
    return this._retval;
  }

  /**
   * 
   */
  public void setReturn(boolean value) {
    this._retval = value;
  }

}
