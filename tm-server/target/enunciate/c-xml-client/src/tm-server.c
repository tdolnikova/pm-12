#include <enunciate-common.c>
#ifndef DEF_tm_server_ns1_project_H
#define DEF_tm_server_ns1_project_H

/**
 * (no documentation provided)
 */
struct tm_server_ns1_project {

};

/**
 * Reads a Project from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Project, or NULL in case of error.
 */
static struct tm_server_ns1_project *xmlTextReaderReadNs1ProjectType(xmlTextReaderPtr reader);

/**
 * Writes a Project to XML.
 *
 * @param writer The XML writer.
 * @param _project The Project to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1ProjectType(xmlTextWriterPtr writer, struct tm_server_ns1_project *_project);

/**
 * Frees the elements of a Project.
 *
 * @param _project The Project to free.
 */
static void freeNs1ProjectType(struct tm_server_ns1_project *_project);

#endif /* DEF_tm_server_ns1_project_H */
#ifndef DEF_tm_server_ns1_session_H
#define DEF_tm_server_ns1_session_H

/**
 * (no documentation provided)
 */
struct tm_server_ns1_session {

};

/**
 * Reads a Session from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Session, or NULL in case of error.
 */
static struct tm_server_ns1_session *xmlTextReaderReadNs1SessionType(xmlTextReaderPtr reader);

/**
 * Writes a Session to XML.
 *
 * @param writer The XML writer.
 * @param _session The Session to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1SessionType(xmlTextWriterPtr writer, struct tm_server_ns1_session *_session);

/**
 * Frees the elements of a Session.
 *
 * @param _session The Session to free.
 */
static void freeNs1SessionType(struct tm_server_ns1_session *_session);

#endif /* DEF_tm_server_ns1_session_H */
#ifndef DEF_tm_server_ns1_task_H
#define DEF_tm_server_ns1_task_H

/**
 * (no documentation provided)
 */
struct tm_server_ns1_task {

};

/**
 * Reads a Task from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Task, or NULL in case of error.
 */
static struct tm_server_ns1_task *xmlTextReaderReadNs1TaskType(xmlTextReaderPtr reader);

/**
 * Writes a Task to XML.
 *
 * @param writer The XML writer.
 * @param _task The Task to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1TaskType(xmlTextWriterPtr writer, struct tm_server_ns1_task *_task);

/**
 * Frees the elements of a Task.
 *
 * @param _task The Task to free.
 */
static void freeNs1TaskType(struct tm_server_ns1_task *_task);

#endif /* DEF_tm_server_ns1_task_H */
#ifndef DEF_tm_server_ns1_user_H
#define DEF_tm_server_ns1_user_H

/**
 * (no documentation provided)
 */
struct tm_server_ns1_user {

};

/**
 * Reads a User from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The User, or NULL in case of error.
 */
static struct tm_server_ns1_user *xmlTextReaderReadNs1UserType(xmlTextReaderPtr reader);

/**
 * Writes a User to XML.
 *
 * @param writer The XML writer.
 * @param _user The User to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1UserType(xmlTextWriterPtr writer, struct tm_server_ns1_user *_user);

/**
 * Frees the elements of a User.
 *
 * @param _user The User to free.
 */
static void freeNs1UserType(struct tm_server_ns1_user *_user);

#endif /* DEF_tm_server_ns1_user_H */
#ifndef DEF_tm_server_ns1_dataType_H
#define DEF_tm_server_ns1_dataType_H

/**
 * (no documentation provided)
 */
enum tm_server_ns1_dataType {

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_NAME,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_DESCRIPTION,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_LOGIN,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_PASSWORD,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_ROLE
};

/**
 * Reads a DataType from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The DataType, or NULL if unable to be read.
 */
static enum tm_server_ns1_dataType *xmlTextReaderReadNs1DataTypeType(xmlTextReaderPtr reader);

/**
 * Writes a DataType to XML.
 *
 * @param writer The XML writer.
 * @param _dataType The DataType to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1DataTypeType(xmlTextWriterPtr writer, enum tm_server_ns1_dataType *_dataType);

/**
 * Frees a DataType.
 *
 * @param _dataType The DataType to free.
 */
static void freeNs1DataTypeType(enum tm_server_ns1_dataType *_dataType);

#endif
#ifndef DEF_tm_server_ns1_project_M
#define DEF_tm_server_ns1_project_M

/**
 * Reads a Project from XML. The reader is assumed to be at the start element.
 *
 * @return the Project, or NULL in case of error.
 */
static struct tm_server_ns1_project *xmlTextReaderReadNs1ProjectType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct tm_server_ns1_project *_project = calloc(1, sizeof(struct tm_server_ns1_project));




  return _project;
}

/**
 * Writes a Project to XML.
 *
 * @param writer The XML writer.
 * @param _project The Project to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1ProjectType(xmlTextWriterPtr writer, struct tm_server_ns1_project *_project) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;

  return totalBytes;
}

/**
 * Frees the elements of a Project.
 *
 * @param _project The Project to free.
 */
static void freeNs1ProjectType(struct tm_server_ns1_project *_project) {
  int i;
}
#endif /* DEF_tm_server_ns1_project_M */
#ifndef DEF_tm_server_ns1_session_M
#define DEF_tm_server_ns1_session_M

/**
 * Reads a Session from XML. The reader is assumed to be at the start element.
 *
 * @return the Session, or NULL in case of error.
 */
static struct tm_server_ns1_session *xmlTextReaderReadNs1SessionType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct tm_server_ns1_session *_session = calloc(1, sizeof(struct tm_server_ns1_session));




  return _session;
}

/**
 * Writes a Session to XML.
 *
 * @param writer The XML writer.
 * @param _session The Session to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1SessionType(xmlTextWriterPtr writer, struct tm_server_ns1_session *_session) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;

  return totalBytes;
}

/**
 * Frees the elements of a Session.
 *
 * @param _session The Session to free.
 */
static void freeNs1SessionType(struct tm_server_ns1_session *_session) {
  int i;
}
#endif /* DEF_tm_server_ns1_session_M */
#ifndef DEF_tm_server_ns1_task_M
#define DEF_tm_server_ns1_task_M

/**
 * Reads a Task from XML. The reader is assumed to be at the start element.
 *
 * @return the Task, or NULL in case of error.
 */
static struct tm_server_ns1_task *xmlTextReaderReadNs1TaskType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct tm_server_ns1_task *_task = calloc(1, sizeof(struct tm_server_ns1_task));




  return _task;
}

/**
 * Writes a Task to XML.
 *
 * @param writer The XML writer.
 * @param _task The Task to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1TaskType(xmlTextWriterPtr writer, struct tm_server_ns1_task *_task) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;

  return totalBytes;
}

/**
 * Frees the elements of a Task.
 *
 * @param _task The Task to free.
 */
static void freeNs1TaskType(struct tm_server_ns1_task *_task) {
  int i;
}
#endif /* DEF_tm_server_ns1_task_M */
#ifndef DEF_tm_server_ns1_user_M
#define DEF_tm_server_ns1_user_M

/**
 * Reads a User from XML. The reader is assumed to be at the start element.
 *
 * @return the User, or NULL in case of error.
 */
static struct tm_server_ns1_user *xmlTextReaderReadNs1UserType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct tm_server_ns1_user *_user = calloc(1, sizeof(struct tm_server_ns1_user));




  return _user;
}

/**
 * Writes a User to XML.
 *
 * @param writer The XML writer.
 * @param _user The User to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1UserType(xmlTextWriterPtr writer, struct tm_server_ns1_user *_user) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;

  return totalBytes;
}

/**
 * Frees the elements of a User.
 *
 * @param _user The User to free.
 */
static void freeNs1UserType(struct tm_server_ns1_user *_user) {
  int i;
}
#endif /* DEF_tm_server_ns1_user_M */
#ifndef DEF_tm_server_ns1_dataType_M
#define DEF_tm_server_ns1_dataType_M

/**
 * Reads a DataType from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The DataType, or NULL if unable to be read.
 */
static enum tm_server_ns1_dataType *xmlTextReaderReadNs1DataTypeType(xmlTextReaderPtr reader) {
  xmlChar *enumValue = xmlTextReaderReadEntireNodeValue(reader);
  enum tm_server_ns1_dataType *value = calloc(1, sizeof(enum tm_server_ns1_dataType));
  if (enumValue != NULL) {
    if (xmlStrcmp(enumValue, BAD_CAST "NAME") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_NAME;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "DESCRIPTION") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_DESCRIPTION;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "LOGIN") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_LOGIN;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "PASSWORD") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_PASSWORD;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "ROLE") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_ROLE;
      free(enumValue);
      return value;
    }
#if DEBUG_ENUNCIATE
    printf("Attempt to read enum value failed: %s doesn't match an enum value.\n", enumValue);
#endif
  }
#if DEBUG_ENUNCIATE
  else {
    printf("Attempt to read enum value failed: NULL value.\n");
  }
#endif

  return NULL;
}

/**
 * Writes a DataType to XML.
 *
 * @param writer The XML writer.
 * @param _dataType The DataType to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1DataTypeType(xmlTextWriterPtr writer, enum tm_server_ns1_dataType *_dataType) {
  switch (*_dataType) {
    case TM_SERVER_NS1_DATATYPE_NAME:
      return xmlTextWriterWriteString(writer, BAD_CAST "NAME");
    case TM_SERVER_NS1_DATATYPE_DESCRIPTION:
      return xmlTextWriterWriteString(writer, BAD_CAST "DESCRIPTION");
    case TM_SERVER_NS1_DATATYPE_LOGIN:
      return xmlTextWriterWriteString(writer, BAD_CAST "LOGIN");
    case TM_SERVER_NS1_DATATYPE_PASSWORD:
      return xmlTextWriterWriteString(writer, BAD_CAST "PASSWORD");
    case TM_SERVER_NS1_DATATYPE_ROLE:
      return xmlTextWriterWriteString(writer, BAD_CAST "ROLE");
  }

#if DEBUG_ENUNCIATE
  printf("Unable to write enum value (no valid value found).\n");
#endif
  return -1;
}

/**
 * Frees a DataType.
 *
 * @param _dataType The DataType to free.
 */
static void freeNs1DataTypeType(enum tm_server_ns1_dataType *_dataType) {
  //no-op
}
#endif /* DEF_tm_server_ns1_dataType_M */
