#import "enunciate-common.h"
#ifndef DEF_TM_SERVERNS1DataType_H
#define DEF_TM_SERVERNS1DataType_H

/**
 * (no documentation provided)
 */
enum TM_SERVERNS1DataType
{

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_NAME,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_DESCRIPTION,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_LOGIN,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_PASSWORD,

  /**
   * (no documentation provided)
   */
  TM_SERVER_NS1_DATATYPE_ROLE
};
/**
 * Reads a DataType from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The DataType, or NULL if unable to be read.
 */
static enum TM_SERVERNS1DataType *xmlTextReaderReadTM_SERVERNS1DataTypeType(xmlTextReaderPtr reader);

/**
 * Writes a DataType to XML.
 *
 * @param writer The XML writer.
 * @param _dataType The DataType to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteTM_SERVERNS1DataTypeType(xmlTextWriterPtr writer, enum TM_SERVERNS1DataType *_dataType);

/**
 * Utility method for getting the enum value for a string.
 *
 * @param _dataType The string to format.
 * @return The enum value or NULL on error.
 */
static enum TM_SERVERNS1DataType *formatStringToTM_SERVERNS1DataTypeType(NSString *_dataType);

/**
 * Utility method for getting the string value of DataType.
 *
 * @param _dataType The DataType to format.
 * @return The string value or NULL on error.
 */
static NSString *formatTM_SERVERNS1DataTypeTypeToString(enum TM_SERVERNS1DataType *_dataType);
#endif /* DEF_TM_SERVERNS1DataType_H */

@class TM_SERVERNS1AbstractEntity;
@class TM_SERVERNS1User;
@class TM_SERVERNS1Task;
@class TM_SERVERNS1Session;
@class TM_SERVERNS1Project;

#ifndef DEF_TM_SERVERNS1AbstractEntity_H
#define DEF_TM_SERVERNS1AbstractEntity_H

/**
 * (no documentation provided)
 */
@interface TM_SERVERNS1AbstractEntity : NSObject
{
  @private
}
@end /* interface TM_SERVERNS1AbstractEntity */

#endif /* DEF_TM_SERVERNS1AbstractEntity_H */
#ifndef DEF_TM_SERVERNS1User_H
#define DEF_TM_SERVERNS1User_H

/**
 * (no documentation provided)
 */
@interface TM_SERVERNS1User : TM_SERVERNS1AbstractEntity
{
  @private
}
@end /* interface TM_SERVERNS1User */

#endif /* DEF_TM_SERVERNS1User_H */
#ifndef DEF_TM_SERVERNS1Task_H
#define DEF_TM_SERVERNS1Task_H

/**
 * (no documentation provided)
 */
@interface TM_SERVERNS1Task : TM_SERVERNS1AbstractEntity
{
  @private
}
@end /* interface TM_SERVERNS1Task */

#endif /* DEF_TM_SERVERNS1Task_H */
#ifndef DEF_TM_SERVERNS1Session_H
#define DEF_TM_SERVERNS1Session_H

/**
 * (no documentation provided)
 */
@interface TM_SERVERNS1Session : TM_SERVERNS1AbstractEntity
{
  @private
}
@end /* interface TM_SERVERNS1Session */

#endif /* DEF_TM_SERVERNS1Session_H */
#ifndef DEF_TM_SERVERNS1Project_H
#define DEF_TM_SERVERNS1Project_H

/**
 * (no documentation provided)
 */
@interface TM_SERVERNS1Project : TM_SERVERNS1AbstractEntity
{
  @private
}
@end /* interface TM_SERVERNS1Project */

#endif /* DEF_TM_SERVERNS1Project_H */
