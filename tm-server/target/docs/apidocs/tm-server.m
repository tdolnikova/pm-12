#import "tm-server.h"
#ifndef DEF_TM_SERVERNS1DataType_M
#define DEF_TM_SERVERNS1DataType_M

/**
 * Reads a DataType from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The DataType, or NULL if unable to be read.
 */
static enum TM_SERVERNS1DataType *xmlTextReaderReadTM_SERVERNS1DataTypeType(xmlTextReaderPtr reader)
{
  xmlChar *enumValue = xmlTextReaderReadEntireNodeValue(reader);
  enum TM_SERVERNS1DataType *value = calloc(1, sizeof(enum TM_SERVERNS1DataType));
  if (enumValue != NULL) {
    if (xmlStrcmp(enumValue, BAD_CAST "NAME") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_NAME;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "DESCRIPTION") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_DESCRIPTION;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "LOGIN") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_LOGIN;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "PASSWORD") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_PASSWORD;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "ROLE") == 0) {
      *value = TM_SERVER_NS1_DATATYPE_ROLE;
      free(enumValue);
      return value;
    }
#if DEBUG_ENUNCIATE
    NSLog(@"Attempt to read enum value failed: %s doesn't match an enum value.", enumValue);
#endif
  }
#if DEBUG_ENUNCIATE
  else {
    NSLog(@"Attempt to read enum value failed: NULL value.");
  }
#endif

  return NULL;
}

/**
 * Utility method for getting the enum value for a string.
 *
 * @param _dataType The string to format.
 * @return The enum value or NULL on error.
 */
enum TM_SERVERNS1DataType *formatStringToTM_SERVERNS1DataTypeType(NSString *_dataType)
{
  enum TM_SERVERNS1DataType *value = calloc(1, sizeof(enum TM_SERVERNS1DataType));
  if ([@"NAME" isEqualToString:_dataType]) {
    *value = TM_SERVER_NS1_DATATYPE_NAME;
  }
  else if ([@"DESCRIPTION" isEqualToString:_dataType]) {
    *value = TM_SERVER_NS1_DATATYPE_DESCRIPTION;
  }
  else if ([@"LOGIN" isEqualToString:_dataType]) {
    *value = TM_SERVER_NS1_DATATYPE_LOGIN;
  }
  else if ([@"PASSWORD" isEqualToString:_dataType]) {
    *value = TM_SERVER_NS1_DATATYPE_PASSWORD;
  }
  else if ([@"ROLE" isEqualToString:_dataType]) {
    *value = TM_SERVER_NS1_DATATYPE_ROLE;
  }
  else{
#if DEBUG_ENUNCIATE
  NSLog(@"Attempt to read enum value failed: %s doesn't match an enum value.", [_dataType UTF8String]);
#endif
    value = NULL;
  }
  return value;
}

/**
 * Writes a DataType to XML.
 *
 * @param writer The XML writer.
 * @param _dataType The DataType to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteTM_SERVERNS1DataTypeType(xmlTextWriterPtr writer, enum TM_SERVERNS1DataType *_dataType)
{
  switch (*_dataType) {
    case TM_SERVER_NS1_DATATYPE_NAME:
      return xmlTextWriterWriteString(writer, BAD_CAST "NAME");
    case TM_SERVER_NS1_DATATYPE_DESCRIPTION:
      return xmlTextWriterWriteString(writer, BAD_CAST "DESCRIPTION");
    case TM_SERVER_NS1_DATATYPE_LOGIN:
      return xmlTextWriterWriteString(writer, BAD_CAST "LOGIN");
    case TM_SERVER_NS1_DATATYPE_PASSWORD:
      return xmlTextWriterWriteString(writer, BAD_CAST "PASSWORD");
    case TM_SERVER_NS1_DATATYPE_ROLE:
      return xmlTextWriterWriteString(writer, BAD_CAST "ROLE");
  }

#if DEBUG_ENUNCIATE
  NSLog(@"Unable to write enum value (no valid value found).");
#endif
  return -1;
}

/**
 * Utility method for getting the string value of DataType.
 *
 * @param _dataType The DataType to format.
 * @return The string value or NULL on error.
 */
static NSString *formatTM_SERVERNS1DataTypeTypeToString(enum TM_SERVERNS1DataType *_dataType)
{
  switch (*_dataType) {
    case TM_SERVER_NS1_DATATYPE_NAME:
      return @"NAME";
    case TM_SERVER_NS1_DATATYPE_DESCRIPTION:
      return @"DESCRIPTION";
    case TM_SERVER_NS1_DATATYPE_LOGIN:
      return @"LOGIN";
    case TM_SERVER_NS1_DATATYPE_PASSWORD:
      return @"PASSWORD";
    case TM_SERVER_NS1_DATATYPE_ROLE:
      return @"ROLE";
    default:
      return NULL;
  }

  return NULL;
}
#endif /* DEF_TM_SERVERNS1DataType_M */
#ifndef DEF_TM_SERVERNS1AbstractEntity_M
#define DEF_TM_SERVERNS1AbstractEntity_M

/**
 * (no documentation provided)
 */
@implementation TM_SERVERNS1AbstractEntity

- (void) dealloc
{
  [super dealloc];
}
@end /* implementation TM_SERVERNS1AbstractEntity */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TM_SERVERNS1AbstractEntity (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TM_SERVERNS1AbstractEntity (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TM_SERVERNS1AbstractEntity (JAXB)

/**
 * Read an instance of TM_SERVERNS1AbstractEntity from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TM_SERVERNS1AbstractEntity defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TM_SERVERNS1AbstractEntity *_tM_SERVERNS1AbstractEntity = [[TM_SERVERNS1AbstractEntity alloc] init];
  NS_DURING
  {
    [_tM_SERVERNS1AbstractEntity initWithReader: reader];
  }
  NS_HANDLER
  {
    _tM_SERVERNS1AbstractEntity = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tM_SERVERNS1AbstractEntity autorelease];
  return _tM_SERVERNS1AbstractEntity;
}

/**
 * Initialize this instance of TM_SERVERNS1AbstractEntity according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TM_SERVERNS1AbstractEntity to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TM_SERVERNS1AbstractEntity (JAXB) */

#endif /* DEF_TM_SERVERNS1AbstractEntity_M */
#ifndef DEF_TM_SERVERNS1User_M
#define DEF_TM_SERVERNS1User_M

/**
 * (no documentation provided)
 */
@implementation TM_SERVERNS1User

- (void) dealloc
{
  [super dealloc];
}
@end /* implementation TM_SERVERNS1User */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TM_SERVERNS1User (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TM_SERVERNS1User (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TM_SERVERNS1User (JAXB)

/**
 * Read an instance of TM_SERVERNS1User from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TM_SERVERNS1User defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TM_SERVERNS1User *_tM_SERVERNS1User = [[TM_SERVERNS1User alloc] init];
  NS_DURING
  {
    [_tM_SERVERNS1User initWithReader: reader];
  }
  NS_HANDLER
  {
    _tM_SERVERNS1User = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tM_SERVERNS1User autorelease];
  return _tM_SERVERNS1User;
}

/**
 * Initialize this instance of TM_SERVERNS1User according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TM_SERVERNS1User to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TM_SERVERNS1User (JAXB) */

#endif /* DEF_TM_SERVERNS1User_M */
#ifndef DEF_TM_SERVERNS1Task_M
#define DEF_TM_SERVERNS1Task_M

/**
 * (no documentation provided)
 */
@implementation TM_SERVERNS1Task

- (void) dealloc
{
  [super dealloc];
}
@end /* implementation TM_SERVERNS1Task */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TM_SERVERNS1Task (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TM_SERVERNS1Task (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TM_SERVERNS1Task (JAXB)

/**
 * Read an instance of TM_SERVERNS1Task from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TM_SERVERNS1Task defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TM_SERVERNS1Task *_tM_SERVERNS1Task = [[TM_SERVERNS1Task alloc] init];
  NS_DURING
  {
    [_tM_SERVERNS1Task initWithReader: reader];
  }
  NS_HANDLER
  {
    _tM_SERVERNS1Task = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tM_SERVERNS1Task autorelease];
  return _tM_SERVERNS1Task;
}

/**
 * Initialize this instance of TM_SERVERNS1Task according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TM_SERVERNS1Task to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TM_SERVERNS1Task (JAXB) */

#endif /* DEF_TM_SERVERNS1Task_M */
#ifndef DEF_TM_SERVERNS1Session_M
#define DEF_TM_SERVERNS1Session_M

/**
 * (no documentation provided)
 */
@implementation TM_SERVERNS1Session

- (void) dealloc
{
  [super dealloc];
}
@end /* implementation TM_SERVERNS1Session */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TM_SERVERNS1Session (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TM_SERVERNS1Session (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TM_SERVERNS1Session (JAXB)

/**
 * Read an instance of TM_SERVERNS1Session from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TM_SERVERNS1Session defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TM_SERVERNS1Session *_tM_SERVERNS1Session = [[TM_SERVERNS1Session alloc] init];
  NS_DURING
  {
    [_tM_SERVERNS1Session initWithReader: reader];
  }
  NS_HANDLER
  {
    _tM_SERVERNS1Session = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tM_SERVERNS1Session autorelease];
  return _tM_SERVERNS1Session;
}

/**
 * Initialize this instance of TM_SERVERNS1Session according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TM_SERVERNS1Session to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TM_SERVERNS1Session (JAXB) */

#endif /* DEF_TM_SERVERNS1Session_M */
#ifndef DEF_TM_SERVERNS1Project_M
#define DEF_TM_SERVERNS1Project_M

/**
 * (no documentation provided)
 */
@implementation TM_SERVERNS1Project

- (void) dealloc
{
  [super dealloc];
}
@end /* implementation TM_SERVERNS1Project */

/**
 * Internal, private interface for JAXB reading and writing.
 */
@interface TM_SERVERNS1Project (JAXB) <JAXBReading, JAXBWriting, JAXBType>

@end /*interface TM_SERVERNS1Project (JAXB)*/

/**
 * Internal, private implementation for JAXB reading and writing.
 */
@implementation TM_SERVERNS1Project (JAXB)

/**
 * Read an instance of TM_SERVERNS1Project from an XML reader.
 *
 * @param reader The reader.
 * @return An instance of TM_SERVERNS1Project defined by the XML reader.
 */
+ (id<JAXBType>) readXMLType: (xmlTextReaderPtr) reader
{
  TM_SERVERNS1Project *_tM_SERVERNS1Project = [[TM_SERVERNS1Project alloc] init];
  NS_DURING
  {
    [_tM_SERVERNS1Project initWithReader: reader];
  }
  NS_HANDLER
  {
    _tM_SERVERNS1Project = nil;
    [localException raise];
  }
  NS_ENDHANDLER

  [_tM_SERVERNS1Project autorelease];
  return _tM_SERVERNS1Project;
}

/**
 * Initialize this instance of TM_SERVERNS1Project according to
 * the XML being read from the reader.
 *
 * @param reader The reader.
 */
- (id) initWithReader: (xmlTextReaderPtr) reader
{
  return [super initWithReader: reader];
}

/**
 * Write the XML for this instance of TM_SERVERNS1Project to the writer.
 * Note that since we're only writing the XML type,
 * No start/end element will be written.
 *
 * @param reader The reader.
 */
- (void) writeXMLType: (xmlTextWriterPtr) writer
{
  [super writeXMLType:writer];
}

//documentation inherited.
- (BOOL) readJAXBAttribute: (xmlTextReaderPtr) reader
{
  void *_child_accessor;

  if ([super readJAXBAttribute: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (BOOL) readJAXBValue: (xmlTextReaderPtr) reader
{
  return [super readJAXBValue: reader];
}

//documentation inherited.
- (BOOL) readJAXBChildElement: (xmlTextReaderPtr) reader
{
  id __child;
  void *_child_accessor;
  int status, depth;

  if ([super readJAXBChildElement: reader]) {
    return YES;
  }

  return NO;
}

//documentation inherited.
- (int) readUnknownJAXBChildElement: (xmlTextReaderPtr) reader
{
  return [super readUnknownJAXBChildElement: reader];
}

//documentation inherited.
- (void) readUnknownJAXBAttribute: (xmlTextReaderPtr) reader
{
  [super readUnknownJAXBAttribute: reader];
}

//documentation inherited.
- (void) writeJAXBAttributes: (xmlTextWriterPtr) writer
{
  int status;

  [super writeJAXBAttributes: writer];

}

//documentation inherited.
- (void) writeJAXBValue: (xmlTextWriterPtr) writer
{
  [super writeJAXBValue: writer];
}

/**
 * Method for writing the child elements.
 *
 * @param writer The writer.
 */
- (void) writeJAXBChildElements: (xmlTextWriterPtr) writer
{
  int status;
  id __item;
  id __item_copy;
  NSEnumerator *__enumerator;

  [super writeJAXBChildElements: writer];

}
@end /* implementation TM_SERVERNS1Project (JAXB) */

#endif /* DEF_TM_SERVERNS1Project_M */
