package com.dolnikova.tm.service;

import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.api.service.ISessionService;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.util.SignatureUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@NoArgsConstructor
public final class SessionService  extends AbstractService<Session> implements ISessionService {

    private ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void createSession(User user) {
        Session session = new Session();
        session.setUserId(user.getId());
        @Nullable final String signature = SignatureUtil.sign(user.getPassword(), "JAVA", 3);
        System.out.println("Создание. Сигнатура " + user.getLogin() + ": " + signature);
        session.setSignature(signature);
        persist(session);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String ownerId, @Nullable String id) {
        if (ownerId == null || ownerId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        return sessionRepository.findOneById(ownerId, id);
    }

    @Nullable
    @Override
    public Session findOneByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return sessionRepository.findOneByUserId(userId);
    }

    @Nullable
    public Session findOneBySignature(@Nullable final String signature) {
        if (signature == null || signature.isEmpty()) return null;
        System.out.println("Поиск сессии в сервисе");
        return sessionRepository.findOneBySignature(signature);
    }

    @Override
    public @Nullable List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return null;
        return sessionRepository.findAll(userId);
    }

    @Override
    public void persist(@Nullable final Session entity) {
        if (entity == null) return;
        sessionRepository.persist(entity);
    }

    @Override
    public void persistList(@Nullable List<Session> list) {
        if (list == null || list.isEmpty()) return;
        sessionRepository.persistList(list);
    }

    @Override
    public void remove(@Nullable final Session entity) {
        if (entity == null) return;
        sessionRepository.remove(entity);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeAll(userId);
    }

    @Override
    public void saveBin(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveBin(entities);
    }

    @Override
    public void saveFasterxmlJson(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveFasterxmlJson(entities);
    }

    @Override
    public void saveFasterxmlXml(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveFasterxmlXml(entities);
    }

    @Override
    public void saveJaxbJson(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveJaxbJson(entities);
    }

    @Override
    public void saveJaxbXml(@Nullable List<Session> entities) throws Exception {
        if(entities == null || entities.isEmpty()) return;
        sessionRepository.saveJaxbXml(entities);
    }

    @Override
    public @Nullable List<Session> loadBin() throws Exception {
        return sessionRepository.loadBin();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlJson() throws Exception {
        return sessionRepository.loadFasterxmlJson();
    }

    @Override
    public @Nullable List<Session> loadFasterxmlXml() throws Exception {
        return sessionRepository.loadFasterxmlXml();
    }

    @Override
    public @Nullable List<Session> loadJaxbJson() throws Exception {
        return sessionRepository.loadJaxbJson();
    }

    @Override
    public @Nullable List<Session> loadJaxbXml() throws Exception {
        return sessionRepository.loadJaxbXml();
    }

}
