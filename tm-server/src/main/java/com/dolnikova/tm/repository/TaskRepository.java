package com.dolnikova.tm.repository;

import com.dolnikova.tm.Application;
import com.dolnikova.tm.api.repository.ITaskRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.constant.DbConstant;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Task;
import com.dolnikova.tm.enumerated.DataType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    private final Logger LOGGER = Logger.getLogger(Application.class.getName());

    @SneakyThrows
    @Nullable
    @Override
    public Task findOneByUserId(@NotNull String id) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            @Nullable final Task task = fetch(resultSet);
            statement.close();
            LOGGER.info("Найдена задача:\n" + task.getId() + "\n" + task.getName() + "\n" + task.getUserId());
            return task;
        }
        return null;
    }

    @SneakyThrows
    @Nullable
    public Task findOneByUserIdAndId(@NotNull String userId, @NotNull String id) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.ID +
                " = ? AND " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            @Nullable final Task task = fetch(resultSet);
            statement.close();
            LOGGER.info("Найдена задача:\n" + task.getId() + "\n" + task.getName() + "\n" + task.getUserId());
            return task;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public Task findOneByName(@NotNull String name) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.NAME +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            @Nullable final Task task = fetch(resultSet);
            statement.close();
            LOGGER.info("Найдена задача:\n" + task.getId() + "\n" + task.getName() + "\n" + task.getUserId());
            return task;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable List<Task> findAll(@NotNull String userId) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        LOGGER.info("Найден список задач:\n");
        for (Task task : result) {
            LOGGER.info(task.getId() + " " + task.getName());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public @Nullable List<Task> findAllByName(@NotNull String userId, @NotNull String text) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.USER_ID +
                " = ? AND " +
                DbConstant.NAME +
                " LIKE " +
                "?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, "%" + text + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        LOGGER.info("Найден список задач:\n");
        for (Task task : result) {
            LOGGER.info(task.getId() + " " + task.getName());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public @Nullable List<Task> findAllByDescription(@NotNull String userId, @NotNull String text) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.USER_ID +
                " = ? AND " +
                DbConstant.DESCRIPTION +
                " LIKE " +
                "?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, "%" + text + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        LOGGER.info("Найден список задач:\n");
        for (Task task : result) {
            LOGGER.info(task.getId() + " " + task.getName());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public void persist(@NotNull Task entity) {
        @NotNull final StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ");
        sql.append(DbConstant.APP_TASK);
        sql.append(" (");
        sql.append(DbConstant.ID);
        sql.append(", ");
        sql.append(DbConstant.NAME);
        sql.append(", ");
        sql.append(DbConstant.PROJECT_ID);
        sql.append(", ");
        sql.append(DbConstant.USER_ID);
        sql.append(") VALUES (?, ?, ?, ?)");

        System.out.println("Добавление задачи:");
        System.out.println("task_id: " + entity.getId());
        System.out.println("name: " + entity.getName());
        System.out.println("project_id: " + entity.getProjectId());
        System.out.println("user_id: " + entity.getUserId());

        @NotNull final PreparedStatement preparedStatement = Bootstrap.connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, entity.getId());
        preparedStatement.setString(2, entity.getName());
        preparedStatement.setString(3, entity.getProjectId());
        preparedStatement.setString(4, entity.getUserId());
        preparedStatement.executeUpdate();
        LOGGER.info("В бд добавлена задача\n" + entity.getId() + "\n" + entity.getName() + "\n" + entity.getUserId());
    }

    @SneakyThrows
    @Override
    public void persistList(@NotNull List<Task> list) {
        for (Task task : list) {
            persist(task);
        }
    }

    @SneakyThrows
    public void merge(@NotNull String newData, @NotNull Task entityToMerge, @NotNull DataType dataType) {
        switch (dataType) {
            case NAME:
                updateName(newData, entityToMerge);
                break;
            case DESCRIPTION:
                updateDescription(newData, entityToMerge);
                break;
        }
    }

    @SneakyThrows
    private void updateName(@NotNull String newData, @NotNull Task entityToMerge) {
        String sql = "UPDATE " +
                DbConstant.APP_TASK +
                " SET " +
                DbConstant.NAME +
                " = ?" +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, newData);
        statement.setString(2, entityToMerge.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Редактирование задачи " + entityToMerge.getUserId() + "\nИзменено имя на: " + newData);
    }

    @SneakyThrows
    private void updateDescription(@NotNull String newData, @NotNull Task entityToMerge) {
        String sql = "UPDATE " +
                DbConstant.APP_TASK +
                " SET " +
                DbConstant.DESCRIPTION +
                " = ?" +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, newData);
        statement.setString(2, entityToMerge.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Редактирование задачи " + entityToMerge.getUserId() + "\nИзменено имя на: " + newData);
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull Task entity) {
        String sql = "DELETE FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @SneakyThrows
    @Override
    public void removeAll(@NotNull String userId) {
        String sql = "DELETE FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Удалены задачи пользователя " + userId);
    }

    @Override
    public void saveBin(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Task> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.TASKS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Task> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Task> tasks = (ArrayList) objectInputStream.readObject();
            return tasks;
        }
    }

    @Nullable
    @Override
    public List<Task> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Task> tasks = objectMapper.readValue(file, ArrayList.class);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Task> tasks = xmlMapper.readValue(file, ArrayList.class);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Task> tasks = (ArrayList<Task>) unmarshaller.unmarshal(file);
        return tasks;
    }

    @Nullable
    @Override
    public List<Task> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.TASKS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Task> tasks = (ArrayList<Task>) unmarshaller.unmarshal(file);
        return tasks;
    }

    @Nullable
    @SneakyThrows
    private Task fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(DbConstant.ID));
        task.setDateBegin(row.getDate(DbConstant.DATE_BEGIN));
        task.setDateEnd(row.getDate(DbConstant.DATE_END));
        task.setName(row.getString(DbConstant.NAME));
        task.setDescription(row.getString(DbConstant.DESCRIPTION));
        task.setUserId(row.getString(DbConstant.USER_ID));
        task.setProjectId(row.getString(DbConstant.PROJECT_ID));
        return task;
    }

}
