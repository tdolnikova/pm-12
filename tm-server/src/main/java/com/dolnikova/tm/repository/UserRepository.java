package com.dolnikova.tm.repository;

import com.dolnikova.tm.Application;
import com.dolnikova.tm.api.repository.IUserRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.constant.DbConstant;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Session;
import com.dolnikova.tm.entity.User;
import com.dolnikova.tm.enumerated.DataType;
import com.dolnikova.tm.enumerated.Role;
import com.dolnikova.tm.util.PasswordHashUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final Logger LOGGER = Logger.getLogger(Application.class.getName());

    @SneakyThrows
    @Nullable
    @Override
    public User findOneByUserId(@NotNull String id) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_USER +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            @Nullable final User user = fetch(resultSet);
            statement.close();
            LOGGER.info("Найден пользователь:\n" + user.getId() + "\n" + user.getLogin() + "\n" + user.getPassword());
            return user;
        }
        return null;
    }

    @SneakyThrows
    @Nullable
    @Override
    public User findOneByLogin(@NotNull String login) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_USER +
                " WHERE " +
                DbConstant.LOGIN +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            @Nullable final User user = fetch(resultSet);
            statement.close();
            LOGGER.info("Найден пользователь:\n" + user.getId() + "\n" + user.getLogin() + "\n" + user.getPassword());
            return user;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable User findOneBySession(@NotNull Session session) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_USER +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, session.getUserId());
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            @Nullable final User user = fetch(resultSet);
            statement.close();
            LOGGER.info("Найден пользователь:\n" + user.getId() + "\n" + user.getLogin() + "\n" + user.getPassword());
            return user;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable List<User> findAll(@NotNull String userId) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_TASK +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        LOGGER.info("Найден список пользователей:\n");
        for (User user : result) {
            LOGGER.info(user.getId() + "\n" + user.getLogin() + "\n" + user.getPassword());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public @Nullable List<User> findAllByLogin(@NotNull String userId, @NotNull String text) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_USER +
                " WHERE " +
                DbConstant.LOGIN +
                " LIKE " +
                "?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, "%" + text + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        LOGGER.info("Найден список пользователей:\n");
        for (User user : result) {
            LOGGER.info(user.getId() + "\n" + user.getLogin() + "\n" + user.getPassword());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public void persist(@NotNull User entity) {
        @NotNull final StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ");
        sql.append(DbConstant.APP_USER);
        sql.append(" (");
        sql.append(DbConstant.ID);
        sql.append(", ");
        sql.append(DbConstant.LOGIN);
        sql.append(", ");
        sql.append(DbConstant.PASSWORD_HASH);
        sql.append(") VALUES (?, ?, ?)");

        @NotNull final PreparedStatement preparedStatement = Bootstrap.connection.prepareStatement(sql.toString());
        System.out.println("Добавление пользователя:");
        preparedStatement.setString(1, entity.getId());
        System.out.println(entity.getId());
        preparedStatement.setString(2, entity.getLogin());
        System.out.println(entity.getLogin());
        preparedStatement.setString(3, entity.getPassword());
        System.out.println(entity.getPassword());
        preparedStatement.executeUpdate();
        LOGGER.info("В бд добавлен пользователь\n" + entity.getId() + "\n" + entity.getLogin() + "\n" + entity.getPassword());
    }

    @SneakyThrows
    @Override
    public void persistList(@NotNull List<User> list) {
        for (User user : list) {
            persist(user);
        }
    }

    @SneakyThrows
    @Override
    public void merge(@NotNull String newData, @NotNull User entityToMerge, @NotNull DataType dataType) {
        System.out.println("Ready?");
        switch (dataType) {
            case LOGIN:
                System.out.println("Updating...");
                updateLogin(newData, entityToMerge);
                break;
            case ROLE:
                updateRole(newData, entityToMerge);
                break;
            case PASSWORD:
                updatePassword(newData, entityToMerge);
                break;
        }
    }

    @SneakyThrows
    private void updatePassword(@NotNull String newData, @NotNull User entityToMerge) {
        String sql = "UPDATE " +
                DbConstant.APP_USER +
                " SET " +
                DbConstant.PASSWORD_HASH +
                " = ?" +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        String hashedInput = PasswordHashUtil.md5(newData);
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, hashedInput);
        statement.setString(2, entityToMerge.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Изменение пароля. ID пользователя: " + entityToMerge.getId() + "\nИзменено имя на: " + newData);
    }

    @SneakyThrows
    private void updateLogin(@NotNull String newData, @NotNull User entityToMerge) {
        String sql = "UPDATE " +
                DbConstant.APP_USER +
                " SET " +
                DbConstant.LOGIN +
                " = ?" +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        System.out.println(sql);
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, newData);
        statement.setString(2, entityToMerge.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Редактирование профиля " + entityToMerge.getId() + "\nИзменено имя на: " + newData);
    }

    @SneakyThrows
    private void updateRole(@NotNull String newData, @NotNull User entityToMerge) {
        String sql = "UPDATE " +
                DbConstant.APP_USER +
                " SET " +
                DbConstant.ROLE +
                " = ?" +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, newData);
        statement.setString(2, entityToMerge.getId());
        statement.executeQuery();
        statement.close();
        LOGGER.info("Редактирование профиля " + entityToMerge.getId() + "\nИзменено имя на: " + newData);
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull User entity) {
        String sql = "DELETE FROM " +
                DbConstant.APP_USER +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Удален пользователь " + entity.getId() + " " + entity.getId());
    }

    @SneakyThrows
    @Override
    public void removeAll(@NotNull String userId) {
        String sql = "DELETE FROM " +
                DbConstant.APP_USER;
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Пользователь " + userId + " удалил всех пользователей");
    }

    @Override
    public boolean checkPassword(@NotNull String userId, @NotNull String userInput) {
        String hashedInput = PasswordHashUtil.md5(userInput);
        System.out.println("Пользовательский пароль: " + userInput);
        System.out.println("Хэшированный пароль: " + hashedInput);
        User user = findOneByUserId(userId);
        if (user != null) {
            String password = user.getPassword();
            System.out.println("Пароль пользователя: " + password);
            return hashedInput.equals(password);
        }
        return false;
    }

    @Override
    public void saveBin(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<User> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.USERS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<User> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<User> users = (ArrayList) objectInputStream.readObject();
            return users;
        }
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<User> users = objectMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<User> users = xmlMapper.readValue(file, ArrayList.class);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

    @Nullable
    @Override
    public List<User> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.USERS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<User> users = (ArrayList<User>) unmarshaller.unmarshal(file);
        return users;
    }

    @Nullable
    @SneakyThrows
    private User fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(DbConstant.ID));
        user.setEmail(row.getString(DbConstant.EMAIL));
        user.setLogin(row.getString(DbConstant.FIRST_NAME));
        user.setLastName(row.getString(DbConstant.LAST_NAME));
        user.setLogin(row.getString(DbConstant.LOGIN));
        user.setMiddleName(row.getString(DbConstant.MIDDLE_NAME));
        user.setPassword(row.getString(DbConstant.PASSWORD_HASH));
        user.setPhone(row.getString(DbConstant.PHONE));
        String role = row.getString(DbConstant.ROLE);
        if (role != null && !role.isEmpty()) user.setRole(Role.valueOf(role));
        //user.setLocked((row.getInt(DbConstant.LOCKED) > 0));
        return user;
    }

}
