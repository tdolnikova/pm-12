package com.dolnikova.tm.repository;

import com.dolnikova.tm.Application;
import com.dolnikova.tm.api.repository.ISessionRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.constant.DbConstant;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Session;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    private final Logger LOGGER = Logger.getLogger(Application.class.getName());

    @SneakyThrows
    @Nullable
    @Override
    public Session findOneByUserId(@NotNull String userId) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_SESSION +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            @Nullable final Session session = fetch(resultSet);
            statement.close();
            LOGGER.info("Найдена сессия: " + session.getId() + "\nСессия принадлежит пользователю: " + session.getUserId());
            return session;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable Session findOneById(@Nullable String ownerId, @Nullable String id) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_SESSION +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            @Nullable final Session session = fetch(resultSet);
            statement.close();
            LOGGER.info("Найдена сессия: " + session.getId() + "\nСессия принадлежит пользователю: " + session.getUserId());
            return session;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable Session findOneBySignature(@Nullable String signature) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_SESSION +
                " WHERE " +
                DbConstant.SIGNATURE +
                " = ?";
        System.out.println("Запрос сессии в репозитории");
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, signature);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            @Nullable final Session session = fetch(resultSet);
            statement.close();
            LOGGER.info("Найдена сессия: " + session.getId() + "\nСессия принадлежит пользователю: " + session.getUserId());
            return session;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable List<Session> findAll(@NotNull String userId) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_SESSION +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        for (Session session : result) {
            LOGGER.info("Найдена сессия:\n" + session.getId() + "\nСессия принадлежит пользователю: " + session.getUserId());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public void persist(@NotNull Session entity) {
        @NotNull final StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ");
        sql.append(DbConstant.APP_SESSION);
        sql.append(" (");
        sql.append(DbConstant.ID);
        sql.append(", ");
        sql.append(DbConstant.SIGNATURE);
        sql.append(", ");
        sql.append(DbConstant.TIMESTAMP);
        sql.append(", ");
        sql.append(DbConstant.USER_ID);
        sql.append(") VALUES (?, ?, ?, ?)");

        @NotNull final PreparedStatement preparedStatement = Bootstrap.connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, entity.getId());
        preparedStatement.setString(2, entity.getSignature());
        preparedStatement.setLong(3, entity.getTimestamp().longValue());
        preparedStatement.setString(4, entity.getUserId());
        preparedStatement.executeUpdate();
        LOGGER.info("Добавлена сессия:\n" + entity.getId() + "\nСессия принадлежит пользователю: " + entity.getUserId());
    }

    @SneakyThrows
    @Override
    public void persistList(@NotNull List<Session> list) {
        for (Session session : list) {
            persist(session);
        }
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull Session entity) {
        String sql = "DELETE FROM " +
                DbConstant.APP_SESSION +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Удалена сессия:\n" + entity.getId() + "\nСессия принадлежит пользователю: " + entity.getUserId());
    }

    @SneakyThrows
    @Override
    public void removeAll(@NotNull String userId) {
        String sql = "DELETE FROM " +
                DbConstant.APP_SESSION +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Удалены все сессии пользователя: " + userId);
    }

    @Override
    public void saveBin(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Session> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.SESSIONS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Session> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Session> sessions = (ArrayList) objectInputStream.readObject();
            return sessions;
        }
    }

    @Nullable
    @Override
    public List<Session> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Session> sessions = objectMapper.readValue(file, ArrayList.class);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Session> sessions = xmlMapper.readValue(file, ArrayList.class);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Session> sessions = (ArrayList<Session>) unmarshaller.unmarshal(file);
        return sessions;
    }

    @Nullable
    @Override
    public List<Session> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.SESSIONS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Session> sessions = (ArrayList<Session>) unmarshaller.unmarshal(file);
        return sessions;
    }

    @Nullable
    @SneakyThrows
    private Session fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(DbConstant.ID));
        session.setUserId(row.getString(DbConstant.USER_ID));
        session.setSignature(row.getString(DbConstant.SIGNATURE));
        return session;
    }

}
