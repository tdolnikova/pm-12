package com.dolnikova.tm.repository;

import com.dolnikova.tm.Application;
import com.dolnikova.tm.api.repository.IProjectRepository;
import com.dolnikova.tm.bootstrap.Bootstrap;
import com.dolnikova.tm.constant.DbConstant;
import com.dolnikova.tm.constant.General;
import com.dolnikova.tm.entity.Project;
import com.dolnikova.tm.enumerated.DataType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private final Logger LOGGER = Logger.getLogger(Application.class.getName());;

    @SneakyThrows
    @Nullable
    @Override
    public Project findOneByUserId(@NotNull String id) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            @Nullable final Project project = fetch(resultSet);
            statement.close();
            LOGGER.info("Найден проект:\n" + project.getId() + "\n" + project.getName() + "\n" + project.getUserId());
            return project;
        }
        return null;
    }

    @SneakyThrows
    @Nullable
    public Project findOneByUserIdAndId(@NotNull String userId, @NotNull String id) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.ID +
                " = ? AND " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, id);
        statement.setString(2, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            @Nullable final Project project = fetch(resultSet);
            statement.close();
            LOGGER.info("Найден проект:\n" + project.getId() + "\n" + project.getName() + "\n" + project.getUserId());
            return project;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public Project findOneByName(@NotNull String name) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.NAME +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if(resultSet.next()) {
            @Nullable final Project project = fetch(resultSet);
            statement.close();
            LOGGER.info("Найден проект:\n" + project.getId() + "\n" + project.getName() + "\n" + project.getUserId());
            return project;
        }
        return null;
    }

    @SneakyThrows
    @Override
    public @Nullable List<Project> findAll(@NotNull String userId) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) return result;
        LOGGER.info("Найден список проектов: [" + result.size() + "]\n");
        for (Project project : result) {
            LOGGER.info(project.getId());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public @Nullable List<Project> findAllByName(@NotNull String userId, @NotNull String text) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.USER_ID +
                " = ? AND " +
                DbConstant.NAME +
                " LIKE " +
                "?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, "%" + text + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        LOGGER.info("Найден список проектов:\n");
        for (Project project : result) {
            LOGGER.info(project.getId() + " " + project.getName());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public @Nullable List<Project> findAllByDescription(@NotNull String userId, @NotNull String text) {
        String sql = "SELECT * FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.USER_ID +
                " = ? AND " +
                DbConstant.DESCRIPTION +
                " LIKE " +
                "?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.setString(2, "%" + text + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        LOGGER.info("Найден список проектов:\n");
        for (Project project : result) {
            LOGGER.info(project.getId() + " " + project.getName());
        }
        return result;
    }

    @SneakyThrows
    @Override
    public void persist(@NotNull Project entity) {
        @NotNull final StringBuilder sql = new StringBuilder();
        sql.append("INSERT INTO ");
        sql.append(DbConstant.APP_PROJECT);
        sql.append(" (");
        sql.append(DbConstant.ID);
        sql.append(", ");
        sql.append(DbConstant.NAME);
        sql.append(", ");
        sql.append(DbConstant.USER_ID);
        sql.append(") VALUES (?, ?, ?)");

        @NotNull final PreparedStatement preparedStatement = Bootstrap.connection.prepareStatement(sql.toString());
        preparedStatement.setString(1, entity.getId());
        preparedStatement.setString(2, entity.getName());
        preparedStatement.setString(3, entity.getUserId());
        preparedStatement.executeUpdate();
        LOGGER.info("В бд добавлен проект\n" + entity.getId() + "\n" + entity.getName() + "\n" + entity.getUserId());
    }

    @SneakyThrows
    @Override
    public void persistList(@NotNull List<Project> list) {
        for (Project project : list) {
            persist(project);
        }
    }

    @SneakyThrows
    public void merge(@NotNull String newData, @NotNull Project entityToMerge, @NotNull DataType dataType) {
        switch (dataType) {
            case NAME:
                updateName(newData, entityToMerge);
                break;
            case DESCRIPTION:
                updateDescription(newData, entityToMerge);
                break;
        }
    }

    @SneakyThrows
    private void updateName(@NotNull String newData, @NotNull Project entityToMerge) {
        String sql = "UPDATE " +
                DbConstant.APP_PROJECT +
                " SET " +
                DbConstant.NAME +
                " = ?" +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, newData);
        statement.setString(2, entityToMerge.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Редактирование проекта " + entityToMerge.getUserId() + "\nИзменено имя на: " + newData);
    }

    @SneakyThrows
    private void updateDescription(@NotNull String newData, @NotNull Project entityToMerge) {
        String sql = "UPDATE " +
                DbConstant.APP_PROJECT +
                " SET " +
                DbConstant.DESCRIPTION +
                " = ?" +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, newData);
        statement.setString(2, entityToMerge.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Редактирование проекта " + entityToMerge.getUserId() + "\nИзменено описание на: " + newData);
    }

    @SneakyThrows
    @Override
    public void remove(@NotNull Project entity) {
        String sql = "DELETE FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, entity.getId());
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Удален проект " + entity.getId() + " " + entity.getName());
    }

    @SneakyThrows
    @Override
    public void removeAll(@NotNull String userId) {
        String sql = "DELETE FROM " +
                DbConstant.APP_PROJECT +
                " WHERE " +
                DbConstant.USER_ID +
                " = ?";
        @NotNull final PreparedStatement statement = Bootstrap.connection.prepareStatement(sql);
        statement.setString(1, userId);
        statement.executeUpdate();
        statement.close();
        LOGGER.info("Удалены проекты пользователя " + userId);
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(DbConstant.ID));
        project.setDateBegin(row.getDate(DbConstant.DATE_BEGIN));
        project.setDateEnd(row.getDate(DbConstant.DATE_END));
        project.setDescription(row.getString(DbConstant.DESCRIPTION));
        project.setName(row.getString(DbConstant.NAME));
        project.setUserId(row.getString(DbConstant.USER_ID));
        LOGGER.info("В бд найден проект:\n" + project.getId() + "\n" + project.getName() + "\n" + project.getUserId());
        return project;
    }

    @Override
    public void saveBin(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        try (@Nullable final ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(path))) {
            objectOutputStream.writeObject(new ArrayList<>(entities));
        }
    }

    @Override
    public void saveFasterxmlJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
    }

    @Override
    public void saveFasterxmlXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(path, new ArrayList<>(entities));
        System.out.println(this.getClass().getName());
    }

    @Override
    public void saveJaxbJson(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Override
    public void saveJaxbXml(@NotNull final List<Project> entities) throws Exception {
        @Nullable final File path = new File(General.SAVE_PATH + General.PROJECTS_XML);
        if (!path.exists()) {
            path.getParentFile().mkdirs();
            path.createNewFile();
        }
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(new ArrayList<>(entities), path);
    }

    @Nullable
    @Override
    public List<Project> loadBin() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_BIN);
        try (@Nullable final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            @Nullable final ArrayList<Project> projects = (ArrayList) objectInputStream.readObject();
            return projects;
        }
    }

    @Nullable
    @Override
    public List<Project> loadFasterxmlJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @Nullable final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final ArrayList<Project> projects = objectMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadFasterxmlXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @Nullable final XmlMapper xmlMapper = new XmlMapper();
        @Nullable final ArrayList<Project> projects = xmlMapper.readValue(file, ArrayList.class);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadJaxbJson() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_JSON);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

    @Nullable
    @Override
    public List<Project> loadJaxbXml() throws Exception {
        @Nullable final File file = new File(General.SAVE_PATH + General.PROJECTS_XML);
        @Nullable final JAXBContext jaxbContext = JAXBContext.newInstance(ArrayList.class);
        @Nullable final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @Nullable final ArrayList<Project> projects = (ArrayList<Project>) unmarshaller.unmarshal(file);
        return projects;
    }

}
