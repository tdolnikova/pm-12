package com.dolnikova.tm.constant;

public class DbConstant {

    public final static String APP_USER = "‘app_user‘";
    public final static String APP_TASK = "‘app_task‘";
    public final static String APP_PROJECT = "‘app_project‘";
    public final static String APP_SESSION = "‘app_session‘";

    public final static String ID = "‘id‘";
    public final static String EMAIL = "‘email‘";
    public final static String FIRST_NAME = "‘firstName‘";
    public final static String LAST_NAME = "‘lastName‘";
    public final static String LOCKED = "‘locked‘";
    public final static String LOGIN = "‘login‘";
    public final static String MIDDLE_NAME = "‘middleName‘";
    public final static String PASSWORD_HASH = "‘passwordHash‘";
    public final static String PHONE = "‘phone‘";
    public final static String ROLE = "‘role‘";
    public final static String DATE_BEGIN = "‘dateBegin‘";
    public final static String DATE_END = "‘dateEnd‘";
    public final static String DESCRIPTION = "‘description‘";
    public final static String NAME = "‘name‘";
    public final static String PROJECT_ID = "‘project_id‘";
    public final static String USER_ID = "‘user_id‘";
    public final static String SIGNATURE = "‘signature‘";
    public final static String TIMESTAMP = "‘timestamp‘";

}
