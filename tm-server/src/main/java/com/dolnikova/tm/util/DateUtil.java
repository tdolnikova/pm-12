package com.dolnikova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class DateUtil {

    @NotNull private static final String DATE_FORMAT = "DD.MM.YYYY";
    @Nullable private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);

    @NotNull
    public static String dateToString(@Nullable final Date date){
        return dateFormat.format(date);
    }

    @Nullable
    public static Date stringToDate(@Nullable final String dateString){
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
