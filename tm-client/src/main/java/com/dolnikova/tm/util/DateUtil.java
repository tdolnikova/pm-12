package com.dolnikova.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public final class DateUtil {

    @NotNull private static final String DATE_FORMAT = "DD.MM.YYYY";
    @Nullable private static final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);

    @NotNull
    public static String dateToString(@Nullable final Date date){
        return dateFormat.format(date);
    }

    @Nullable
    public static Date stringToDate(@Nullable final String dateString){
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static XMLGregorianCalendar dateToXMLGregorianCalendar(@Nullable final Date date){
        @NotNull final GregorianCalendar gCalendar = new GregorianCalendar();
        gCalendar.setTime(date);
        @Nullable XMLGregorianCalendar xmlCalendar = null;
        try {
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (DatatypeConfigurationException ex) {
            ex.printStackTrace();
        }
        return xmlCalendar;
    }

    @Nullable
    public static Date XMLGregorianCalendarToDate(@Nullable final XMLGregorianCalendar calendar){
        if(calendar == null) {
            return null;
        }
        return calendar.toGregorianCalendar().getTime();
    }

    @Nullable
    public static XMLGregorianCalendar stringToXMLGregorianCalendar(@Nullable final String dateString){
        @NotNull final GregorianCalendar gCalendar = new GregorianCalendar();
        @Nullable XMLGregorianCalendar xmlCalendar = null;
        try {
            Date date = dateFormat.parse(dateString);
            gCalendar.setTime(date);
            xmlCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gCalendar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xmlCalendar;
    }

}
